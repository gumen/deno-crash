const encoder = new TextEncoder()

const greetText = encoder.encode('Hello World\nMy name is irek\n')

await Deno.writeFile('greet.tmp', greetText)
