let products = [
  {
    id: 0,
    name: 'Product Zero',
    description: 'This is product 0',
    price: 0,
  },
  {
    id: 1,
    name: 'Product One',
    description: 'This is product 1',
    price: 11.11,
  },
  {
    id: 2,
    name: 'Product Two',
    description: 'This is product 2',
    price: 22.22,
  },
  {
    id: 3,
    name: 'Product Three',
    description: 'This is product 3',
    price: 33.33,
  },
  {
    id: 4,
    name: 'Product Four',
    description: 'This is product 4',
    price: 44.44,
  },
]

const getProducts = ({ response }) => {
  response.status = 200
  response.body = products
}

const getProduct = ({ params, response }) => {
  const product = products.find(p => p.id === Number(params.id))

  if (!product) {
    response.status = 404
    return
  }

  response.status = 200
  response.body = product
}

const addProduct = async ({ request, response }) => {
  if (!request.hasBody) {
    response.status = 400
    return
  }

  const body = await request.body()
  const product = body.value
  product.id = products.reduce((max, {id}) => Math.max(max, id), 0) + 1
  products.push(product)
  response.status = 201
  response.body = product
}

const updateProduct = async ({ params, request, response }) => {
  let index;
  let i = products.length;

  while (i--)
    if (products[i].id === Number(params.id)) {
      index = i
      break
    }

  if (index === undefined) {
    response.status = 404
    return
  }

  if (!request.hasBody) {
    response.status = 400
    return
  }

  const body = await request.body()

  if (body.value.name)
    products[index].name = body.value.name

  if (body.value.description)
    products[index].description = body.value.description

  if (body.value.price)
    products[index].price = body.value.price

  response.status = 200
  response.body = products[index]
}

const deleteProduct = ({ params, response }) => {
  let index;
  let i = products.length;

  while (i--)
    if (products[i].id === Number(params.id)) {
      index = i
      break
    }

  if (index === undefined) {
    response.status = 404
    return
  }

  const removedProduct = products.splice(index, 1)

  response.status = 200
  response.body = removedProduct
}


export {
  getProducts,
  getProduct,
  addProduct,
  updateProduct,
  deleteProduct,
}
